package org.venus.library.view.loadmore;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.venus.library.R;
import org.venus.library.utils.TVUtil;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/27
 * Note        recyclerView 下拉刷新/上拉加载
 */
public class LoadMoreLayout extends RelativeLayout {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private RelativeLayout mCLoadShow;
    private TextView mTvLoadMore;
    private RelativeLayout mCNothing;
    private TextView mTvNothing;

    private boolean loadingMore = false;

    private LinearLayoutManager mLinearLayoutManager;

    public LoadMoreLayout(Context context) {
        //super(context);
        this(context, null);
    }

    public LoadMoreLayout(Context context, AttributeSet attrs) {
        //super(context, attrs);
        this(context, attrs, 0);
    }

    public LoadMoreLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.load_more_layout, this);

        initView();

        initEvent();
    }

    private void initEvent() {
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onRefreshStart();
            }
        });
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        if (loadingMore == true) {
                            return;
                        }
                        int itemCount = mLinearLayoutManager.getItemCount();
                        int firstPosition = mLinearLayoutManager.findFirstCompletelyVisibleItemPosition();
                        int lastPosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                        if (firstPosition == 0) {
                            return;
                        }
                        if (lastPosition == itemCount - 1) {
                            onLoadMoreStart();
                        }
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
    }

    private void initView() {
        mSwipeRefreshLayout = findViewById(R.id.swipe);
        mRecyclerView = findViewById(R.id.list);
        mCLoadShow = findViewById(R.id.c_load_show);
        mTvLoadMore = findViewById(R.id.tv_load_more);
        mCNothing = findViewById(R.id.c_nothing);
        mTvNothing = findViewById(R.id.tv_nothing);

        loadingMore = false;
        mCLoadShow.setVisibility(GONE);
        mCNothing.setVisibility(GONE);

        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);

        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.app_color_theme_9), getResources().getColor(R.color.app_color_theme_3), getResources().getColor(R.color.app_color_theme_6));
    }

    public void setRecyclerViewAdapter(RecyclerView.Adapter adapter) {
        mRecyclerView.setAdapter(adapter);
    }

    public void onRefreshStart() {
        if (mOnEventListener != null) {
            mOnEventListener.onRefresh();
        }
    }

    public void onRefreshStop() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    public void onLoadMoreStart() {
        loadingMore = true;
        mCLoadShow.setVisibility(VISIBLE);
        if (mOnEventListener != null) {
            mOnEventListener.onLoadMore();
        }
        scrollToLast();
    }

    public void onLoadMoreStop() {
        loadingMore = false;
        mCLoadShow.setVisibility(GONE);
    }

    public void setLoadText(String text) {
        TVUtil.setText(mTvLoadMore, text);
    }

    private void setNothingText(String text) {
        TVUtil.setText(mTvLoadMore, text);
    }

    public void scrollToLast() {
        mLinearLayoutManager.scrollToPosition(mLinearLayoutManager.getItemCount() - 1);
    }

    public RecyclerView getRecyclerView(){
        return mRecyclerView;
    }

    public LinearLayoutManager getLinearLayoutManager(){
        return mLinearLayoutManager;
    }

    private void showNothing(boolean show) {
        mCNothing.setVisibility(show ? VISIBLE : GONE);
    }

    public interface OnEventListener {
        void onRefresh();

        void onLoadMore();
    }

    private OnEventListener mOnEventListener;

    public void setOnEventListener(OnEventListener onEventListener) {
        mOnEventListener = onEventListener;
    }
}
