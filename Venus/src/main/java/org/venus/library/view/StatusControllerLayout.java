package org.venus.library.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.venus.library.R;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/1/22
 * Note        状态控制器
 */
public class StatusControllerLayout extends RelativeLayout {

    public static final int STATUS_NOTHING = 0;
    public static final int STATUS_LOADING = 1;
    public static final int STATUS_ERROR_NET = 2;
    public static final int STATUS_UNUSUAL_ERROR = 3;
    public static final int STATUS_EMPTY_DATA = 4;
    public static final int STATUS_MESSAGE = 5;
    public static final int STATUS_ERROR_SERVER = 6;
    public static final int STATUS_ERROR_DATA = 7;


    public StatusControllerLayout(Context context) {
        super(context);
    }

    public StatusControllerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StatusControllerLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void addCustomView(View view) {
        changeStatus(STATUS_NOTHING);
        ViewGroup parent = (ViewGroup) view.getParent();
        if (parent != null) {
            parent.removeView(view);
        }

        view.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

        addView(view);
    }

    public void changeStatus(int status) {
        changeStatus(status,null);
    }

    public void changeStatus(int status, Object msg) {
        removeAllViews();
        if (status == STATUS_LOADING) {
            addLoadingView(msg);
        } else if (status == STATUS_ERROR_NET) {
            addNetErrorView(msg);
        } else if (status == STATUS_UNUSUAL_ERROR) {
            addUnusualDataView(msg);
        } else if (status == STATUS_EMPTY_DATA) {
            addEmptyDataView(msg);
        } else if (status == STATUS_MESSAGE) {
            addMessageView(msg);
        } else if (status == STATUS_ERROR_SERVER){
            addErrorServerView(msg);
        } else if (status == STATUS_ERROR_DATA){
            addErrorDataView(msg);
        }
    }

    private void addErrorDataView(Object message) {
        ImageView iv = new ImageView(getContext());
        addView(iv);
        iv.setId(R.id.empty_data_view_iv);
        LayoutParams layoutParams = new LayoutParams(100, 100);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        iv.setBackgroundResource(R.drawable.icon_error_data);
        iv.setLayoutParams(layoutParams);

        TextView tv = new TextView(getContext());
        addView(tv);
        tv.setText("数据错误");
        if (message != null) {
            tv.setText(message.toString());
        }
        LayoutParams tvLayoutParams = (LayoutParams) tv.getLayoutParams();
        tvLayoutParams.addRule(RelativeLayout.BELOW, R.id.empty_data_view_iv);
        tvLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        tvLayoutParams.setMargins(0, 10, 0, 0);
        tv.setLayoutParams(tvLayoutParams);
    }

    private void addErrorServerView(Object message) {
        ImageView iv = new ImageView(getContext());
        addView(iv);
        iv.setId(R.id.empty_data_view_iv);
        LayoutParams layoutParams = new LayoutParams(100, 100);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        iv.setBackgroundResource(R.drawable.icon_error_server);
        iv.setLayoutParams(layoutParams);

        TextView tv = new TextView(getContext());
        addView(tv);
        tv.setText("服务器错误");
        if (message != null) {
            tv.setText(message.toString());
        }
        LayoutParams tvLayoutParams = (LayoutParams) tv.getLayoutParams();
        tvLayoutParams.addRule(RelativeLayout.BELOW, R.id.empty_data_view_iv);
        tvLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        tvLayoutParams.setMargins(0, 10, 0, 0);
        tv.setLayoutParams(tvLayoutParams);
    }

    private void addEmptyDataView(Object message) {
        ImageView iv = new ImageView(getContext());
        addView(iv);
        iv.setId(R.id.empty_data_view_iv);
        LayoutParams layoutParams = new LayoutParams(100, 100);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        iv.setBackgroundResource(R.drawable.icon_empty);
        iv.setLayoutParams(layoutParams);

        TextView tv = new TextView(getContext());
        addView(tv);
        tv.setText("暂无数据");
        if (message != null) {
            tv.setText(message.toString());
        }
        LayoutParams tvLayoutParams = (LayoutParams) tv.getLayoutParams();
        tvLayoutParams.addRule(RelativeLayout.BELOW, R.id.empty_data_view_iv);
        tvLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        tvLayoutParams.setMargins(0, 10, 0, 0);
        tv.setLayoutParams(tvLayoutParams);
    }

    private void addUnusualDataView(Object message) {
        ImageView iv = new ImageView(getContext());
        addView(iv);
        iv.setId(R.id.error_view_iv);
        LayoutParams layoutParams = new LayoutParams(100, 100);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        iv.setBackgroundResource(R.drawable.icon_unusual);
        iv.setLayoutParams(layoutParams);

        TextView tv = new TextView(getContext());
        addView(tv);
        tv.setText("程序出现异常,请联系管理员");
        if (message != null) {
            tv.setText(message.toString());
        }
        LayoutParams tvLayoutParams = (LayoutParams) tv.getLayoutParams();
        tvLayoutParams.addRule(RelativeLayout.BELOW, R.id.error_view_iv);
        tvLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        tvLayoutParams.setMargins(0, 10, 0, 0);
        tv.setLayoutParams(tvLayoutParams);
    }

    private void addNetErrorView(Object message) {
        ImageView iv = new ImageView(getContext());
        addView(iv);
        iv.setId(R.id.error_view_iv);
        LayoutParams layoutParams = new LayoutParams(100, 100);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        iv.setBackgroundResource(R.drawable.icon_net_error);
        iv.setLayoutParams(layoutParams);

        TextView tv = new TextView(getContext());
        addView(tv);
        tv.setText("网络错误...");
        if (message != null) {
            tv.setText(message.toString());
        }
        LayoutParams tvLayoutParams = (LayoutParams) tv.getLayoutParams();
        tvLayoutParams.addRule(RelativeLayout.BELOW, R.id.error_view_iv);
        tvLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        tvLayoutParams.setMargins(0, 10, 0, 0);
        tv.setLayoutParams(tvLayoutParams);
    }

    private void addLoadingView(Object message) {
        ProgressBar progressBar = new ProgressBar(getContext());
        progressBar.setId(R.id.loading_view_progress_bar);
        addView(progressBar);
        LayoutParams layoutParams = new LayoutParams(100, 100);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        progressBar.setLayoutParams(layoutParams);

        TextView tv = new TextView(getContext());
        addView(tv);
        tv.setText("加载中...");
        if (message != null) {
            tv.setText(message.toString());
        }
        LayoutParams tvLayoutParams = (LayoutParams) tv.getLayoutParams();
        tvLayoutParams.addRule(RelativeLayout.BELOW, R.id.loading_view_progress_bar);
        tvLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        tvLayoutParams.setMargins(0, 10, 0, 0);
        tv.setLayoutParams(tvLayoutParams);
    }

    private void addMessageView(Object message) {
        ImageView iv = new ImageView(getContext());
        addView(iv);
        iv.setId(R.id.error_view_iv);
        LayoutParams layoutParams = new LayoutParams(100, 100);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        iv.setBackgroundResource(R.drawable.icon_hint);
        iv.setLayoutParams(layoutParams);

        TextView tv = new TextView(getContext());
        addView(tv);
        if (message == null) {
            message = "提示信息";
        }
        tv.setText(String.valueOf(message));
        LayoutParams tvLayoutParams = (LayoutParams) tv.getLayoutParams();
        tvLayoutParams.addRule(RelativeLayout.BELOW, R.id.error_view_iv);
        tvLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        tvLayoutParams.setMargins(0, 10, 0, 0);
        tv.setLayoutParams(tvLayoutParams);
    }
}
