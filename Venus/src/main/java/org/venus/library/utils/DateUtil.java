package org.venus.library.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.hutool.core.date.DateTime;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/3/19
 * Note        TODO
 */
public class DateUtil {

    /**
     * 获取当前时间的秒值
     *
     * @return
     */
    public static Long currentSecond() {
        Calendar instance = Calendar.getInstance();
        int hour = instance.get(Calendar.HOUR_OF_DAY);
        int minute = instance.get(Calendar.MINUTE);
        int second = instance.get(Calendar.SECOND);

        return second + minute * 60 + hour * 60 * 60L;
    }

    /**
     * 时间格式化
     *
     * @param time
     * @return
     */
    public static String format(long time) {
        return format(time, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 时间格式化
     *
     * @param time
     * @param format
     * @return
     */
    public static String format(long time, String format) {
        return new SimpleDateFormat(format).format(time);
    }

    /**
     * 时间反格式化
     *
     * @param time
     * @param format
     * @return
     */
    public static Long unFormat(String time, String format) throws ParseException {
        System.out.println("转换的时间戳:"+time);
        SimpleDateFormat df = new SimpleDateFormat(format);
        Date parse = df.parse(time);
        long t = parse.getTime();
        System.out.println("转换结果:"+t);
        return t;
    }

    /**
     * 获取现在时间星期日期
     *
     * @return
     */
    public static String getCurrentWeek() {
        return getWeek(System.currentTimeMillis());
    }

    /**
     * 根据时间戳获取星期日期
     *
     * @param time
     * @return
     */
    private static String getWeek(long time) {
        String week = "";
        Calendar calendar = Calendar.getInstance();
        int i = calendar.get(Calendar.DAY_OF_WEEK);

        switch (i) {
            case 1:
                week = "星期日";
                break;
            case 2:
                week = "星期一";
                break;
            case 3:
                week = "星期二";
                break;
            case 4:
                week = "星期三";
                break;
            case 5:
                week = "星期四";
                break;
            case 6:
                week = "星期五";
                break;
            case 7:
                week = "星期六";
                break;
        }

        return week;
    }

    /**
     * 获取当前是第几周
     *
     * @return
     */
    public static int getCurrentWeekCount() {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        calendar.setMinimalDaysInFirstWeek(7);
        calendar.setTime(new Date());
        return calendar.get(Calendar.WEEK_OF_YEAR);

    }
}
