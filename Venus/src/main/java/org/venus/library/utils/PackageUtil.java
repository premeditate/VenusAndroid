package org.venus.library.utils;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.RequiresApi;

import java.util.List;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/12/3
 * Note        TODO
 */
public class PackageUtil {

    /**
     * 打开一个app
     *
     * @param context
     * @param packageName
     * @param launcherActivityClass
     */
    public static void openApp(Context context, String packageName, String launcherActivityClass) {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        ComponentName cn = new ComponentName(packageName, launcherActivityClass);
        intent.setComponent(cn);
        context.startActivity(intent);
    }

    /**
     * 打开一个app
     *
     * @param context
     * @param packageName
     */
    public static void openApp(Context context, String packageName) throws Exception {

        PackageInfo pi = context.getPackageManager().getPackageInfo(packageName, 0);
        Intent resolveIntent = new Intent(Intent.ACTION_MAIN, null);
        resolveIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        resolveIntent.setPackage(pi.packageName);

        PackageManager pm = context.getPackageManager();
        List<ResolveInfo> apps = pm.queryIntentActivities(resolveIntent, 0);

        ResolveInfo ri = apps.iterator().next();
        if (ri != null) {
            String thisPackageName = ri.activityInfo.packageName;
            String thisClassName = ri.activityInfo.name;

            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);

            ComponentName cn = new ComponentName(thisPackageName, thisClassName);

            intent.setComponent(cn);
            context.startActivity(intent);
        } else {
            throw new NullPointerException(packageName + "-没有找到对应的App");
        }
    }

    /**
     * 判断某个Activity 界面是否在前台
     *
     * @param context
     * @param className 某个界面名称
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    public static boolean isForeground(Context context, String className) {
        if (context == null || TextUtils.isEmpty(className)) {
            return false;
        }

        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(1);
        if (list != null && list.size() > 0) {
            ComponentName cpn = list.get(0).topActivity;
            if (className.equals(cpn.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断一个activity是否在前台显示
     *
     * @param context
     * @param className
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    public static boolean activityIsRunningUp(Context context, String className) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> list = am.getRunningTasks(1);
        if (list != null && list.size() > 0) {
            ActivityManager.RunningTaskInfo taskInfo = list.get(0);
            ComponentName topActivity = taskInfo.topActivity;
            if (className.equals(topActivity.getClassName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断某一UID的程序是否有正在运行的进程
     *
     * @param context
     * @param uid
     * @return
     */
    public static boolean processIsRunning(Context context, int uid) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> runningServices = am.getRunningServices(Integer.MAX_VALUE);

        if (runningServices.size() > 0) {
            for (ActivityManager.RunningServiceInfo runningService : runningServices) {
                if (uid == runningService.uid) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 获取应用程序的UID
     *
     * @param context
     * @param packageName
     * @return
     */
    public static int getPackageUid(Context context, String packageName) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(packageName, 0);
            if (applicationInfo != null) {
                return applicationInfo.uid;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return -1;
        }
        return -1;
    }


    /**
     * 应用程序是否运行
     *
     * @param context
     * @param packageName
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    public static boolean appIsRunning(Context context, String packageName) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTasks = am.getRunningTasks(Integer.MAX_VALUE);

        if (runningTasks.size() <= 0) {
            return false;
        }
        for (ActivityManager.RunningTaskInfo runningTask : runningTasks) {
            if (runningTask.baseActivity.getPackageName().equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 判断当前应用程序是否正在前台运行
     *
     * @param context
     * @return
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    public static boolean appIsRunningUp(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTasks = am.getRunningTasks(Integer.MAX_VALUE);
        ComponentName topActivity = runningTasks.get(0).topActivity;
        String currentPackageName = topActivity.getPackageName();
        if (currentPackageName != null && currentPackageName.equals(context.getPackageName())) {
            return true;
        }
        return false;
    }

    /**
     * 判断指定服务是否已经开启
     *
     * @param context
     * @param clazz
     * @return
     */
    public static boolean serviceIsRunning(Context context, Class clazz) {
        return serviceIsRunning(context, clazz.getName());

    }

    /**
     * 检测服务并且启动服务
     *
     * @param context
     * @param clazz
     */
    public static void checkAndRunningService(Context context, Class clazz) {
        if (!serviceIsRunning(context, clazz)) {
            context.startService(new Intent(context, clazz));
        }
    }

    /**
     * 判断指定服务是否已经开启
     *
     * @param context
     * @param serviceName
     * @return
     */
    public static boolean serviceIsRunning(Context context, String serviceName) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> runningServices = am.getRunningServices(Integer.MAX_VALUE);
        if (!(runningServices.size() > 0)) {
            return false;
        }
        for (ActivityManager.RunningServiceInfo info : runningServices) {
            ComponentName service = info.service;
            if (service.getClassName().equals(serviceName)) {
                return true;
            }
        }
        return false;
    }
}
