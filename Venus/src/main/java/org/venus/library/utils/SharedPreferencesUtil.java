package org.venus.library.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/6/17
 * Note        TODO
 */
public class SharedPreferencesUtil {

    private static final String NAME = "Venus";

    public static void setString(Context context, String key, String value){
        SharedPreferences sp = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        sp.edit().putString(key,value).commit();
    }

    public static String getString(Context context, String key,String defaultValue){
        SharedPreferences sp = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return sp.getString(key,defaultValue);
    }

    public static void setLong(Context context, String key, Long value){
        SharedPreferences sp = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        sp.edit().putLong(key,value).commit();
    }

    public static long getLong(Context context, String key,long defaultValue){
        SharedPreferences sp = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return sp.getLong(key,defaultValue);
    }

    public static void setInt(Context context, String key, Integer value){
        SharedPreferences sp = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        sp.edit().putInt(key,value).commit();
    }

    public static int getInt(Context context, String key,int defaultValue){
        SharedPreferences sp = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return sp.getInt(key,defaultValue);
    }

    public static void setFloat(Context context, String key, Float value){
        SharedPreferences sp = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        sp.edit().putFloat(key,value).commit();
    }

    public static Float getFloat(Context context, String key,Float defaultValue){
        SharedPreferences sp = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return sp.getFloat(key,defaultValue);
    }

    public static void setBoolean(Context context, String key, Boolean value){
        SharedPreferences sp = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        sp.edit().putBoolean(key,value).commit();
    }

    public static boolean getBoolean(Context context, String key,boolean defaultValue){
        SharedPreferences sp = context.getSharedPreferences(NAME, Context.MODE_PRIVATE);
        return sp.getBoolean(key,defaultValue);
    }
}
