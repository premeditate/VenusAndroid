package org.venus.library.utils;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/29
 * Note        TODO
 */
public class MathUtil {

    public static String loseLastZero(Double d) {
        if (d != null) {
            return loseLastZero(String.valueOf(d));
        }
        return null;
    }

    public static String loseLastZero(String s) {
        if (null != s && s.indexOf(".") > 0) {
            s = s.replaceAll("0+?$", "");//去掉多余的0
            s = s.replaceAll("[.]$", "");//如最后一位是.则去掉
        }
        return s;
    }
}
