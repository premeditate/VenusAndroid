package org.venus.library.utils;

import android.widget.TextView;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/5/13
 * Note        TODO
 */
public class TVUtil {

    public static void setText(TextView tv,Object text){
        if (tv==null){
            return;
        }
        if (text==null){
            text ="-";
        }

        //如果是小数就保留2位
        if (text instanceof Double){
            text = String.format("%.2f", text);
        }

        tv.setText(String.valueOf(text));
    }
}
