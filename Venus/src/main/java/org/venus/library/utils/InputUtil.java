package org.venus.library.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;


public class InputUtil {

    /**
     * 获取输入内容
     *
     * @param editText 输入框
     * @return
     */
    public static String getInput(EditText editText) {

        if (editText == null){
            throw new NullPointerException("EditText must be not null obj.");
        }

        String trim = editText.getText().toString().trim();

        if (trim == null) {
            return null;
        } else if ("".equals(trim)) {
            return null;
        } else {
            return trim;
        }
    }

    /**
     * 显示界面输入法
     *
     * @param activity
     * @throws Exception
     */
    public static void showSoftInput(Activity activity) throws Exception {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);

        imm.showSoftInput(activity.getCurrentFocus(), InputMethodManager.SHOW_FORCED);

    }

    /**
     * 隐藏界面输入法
     *
     * @param activity
     * @throws Exception
     */
    public static void hindSoftInput(Activity activity) throws Exception {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * dialog退出时隐藏输入法,这个不能在setOnDismissListener()中使用,因为这个调用时dialog已经退出,需要在创建dialog时重写dismiss()
     *
     * @param context
     * @param dialog
     * @throws Exception
     */
    public static void hindSoftInput(Context context, Dialog dialog) throws Exception {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = dialog.getCurrentFocus();
        if (view != null) {
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
