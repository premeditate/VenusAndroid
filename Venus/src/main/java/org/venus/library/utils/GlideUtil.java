package org.venus.library.utils;

import android.app.Activity;
import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import org.venus.library.R;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/1/25
 * Note        TODO
 */
public class GlideUtil {

    public static void loadImage(Activity activity, Object url, ImageView iv){
        Glide.with(activity)
                .load(url)
                .placeholder(R.drawable.icon_loading)
                .error(R.drawable.icon_load_error)
                .into(iv);
    }

    public static void loadImage(Context context, Object url, ImageView iv){
        Glide.with(context)
                .load(url)
                .placeholder(R.drawable.icon_loading)
                .error(R.drawable.icon_load_error)
                .into(iv);
    }
}
