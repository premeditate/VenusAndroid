package org.venus.library.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import androidx.core.content.FileProvider;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/12/13
 * Note        TODO
 */
public class FileUtil {

    /**
     * 下载文件
     *
     * @param url,文件下载的网络地址
     * @param filePath,下载文件存储的文件夹路径
     * @param fileName,下载文件存储的文件名称
     * @param listener,下载过程监听,进度,完成,异常等
     */
    public static void download(String url, String filePath, String fileName, OnDownloadListener listener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                long startTime = System.currentTimeMillis();

                HttpURLConnection conn = null;
                InputStream is = null;
                FileOutputStream fos = null;
                try {

                    File dir = new File(filePath);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    long total = 0;
                    long down = 0;
                    File file = new File(dir, fileName);
                    if (file.exists()) {
                        if (listener != null) {
                            listener.exist(file.getAbsolutePath());
                        }
                        return;
                    } else {
                        file.createNewFile();
                        down = file.length();
                    }

                    conn = (HttpURLConnection) new URL(url).openConnection();
                    conn.setReadTimeout(10000);
                    conn.setConnectTimeout(10000);
                    conn.setRequestProperty("RANGE", "bytes=" + down + "-");
                    conn.setRequestProperty("Charset", "UTF-8");
                    conn.connect();

                    total = conn.getContentLength();

                    is = conn.getInputStream();
                    fos = new FileOutputStream(file, down > 0);
                    byte[] bytes = new byte[1024];
                    int len;
                    if (listener != null) {
                        listener.start(startTime);
                    }
                    while ((len = is.read(bytes)) > 0) {
                        fos.write(bytes, 0, len);
                        down += len;
                        if (listener != null) {
                            listener.pro(total, down);
                        }
                    }
                    fos.flush();
                    fos.getFD().sync();
                    if (listener != null) {
                        listener.done(file.getAbsolutePath());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (listener != null) {
                        listener.error(e);
                    }
                } finally {
                    if (fos != null) {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (is != null) {
                        try {
                            is.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (conn != null) {
                        conn.disconnect();
                    }

                    long endTime = System.currentTimeMillis();
                    if (listener != null) {
                        listener.finish(startTime, endTime);
                    }
                }
            }
        }).start();
    }

    public interface OnDownloadListener {
        void exist(String filePath);

        void start(long startTime);

        void pro(long total, long pro);

        void done(String filePath);

        void error(Exception e);

        void finish(long startTime, long endTime);
    }

    /**
     * 在设备中选择程序打开本地文件
     *
     * @param context
     * @param filePath,本地文件路径
     */
    public static void openFile(Context context, String filePath) {
        File file = new File(filePath);
        Uri uri = FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file);

        Intent intent = new Intent();
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, getMIMEType(file));
        context.startActivity(intent);
    }

    /**
     * 获取文件mime类型
     *
     * @param file
     * @return
     */
    private static String getMIMEType(File file) {
        String type = "*/*";
        String fName = file.getName();

        int dotIndex = fName.lastIndexOf(".");
        if (dotIndex < 0) {
            return type;
        }

        String fileType = fName.substring(dotIndex, fName.length()).toLowerCase();
        if (fileType == null || "".equals(fileType)) {
            return type;
        }

        for (int i = 0; i < MIME_MapTable.length; i++) {
            if (fileType.equals(MIME_MapTable[i][0])) {
                type = MIME_MapTable[i][1];
            }
        }
        return type;
    }

    /**
     * 文件mime类型
     */
    private static final String[][] MIME_MapTable = {
            {".3gp", "video/3gpp"},
            {".apk", "application/vnd.android.package-archive"},
            {".asf", "video/x-ms-asf"},
            {".avi", "video/x-msvideo"},
            {".bin", "application/octet-stream"},
            {".bmp", "image/bmp"},
            {".c", "text/plain"},
            {".class", "application/octet-stream"},
            {".conf", "text/plain"},
            {".cpp", "text/plain"},
            {".csv", "text/csv"},
            {".doc", "application/msword"},
            {".exe", "application/octet-stream"},
            {".gif", "image/gif"},
            {".gtar", "application/x-gtar"},
            {".gz", "application/x-gzip"},
            {".h", "text/plain"},
            {".htm", "text/html"},
            {".html", "text/html"},
            {".jar", "application/java-archive"},
            {".java", "text/plain"},
            {".jpeg", "image/jpeg"},
            {".jpg", "image/jpeg"},
            {".js", "application/x-javascript"},
            {".log", "text/plain"},
            {".m3u", "audio/x-mpegurl"},
            {".m4a", "audio/mp4a-latm"},
            {".m4b", "audio/mp4a-latm"},
            {".m4p", "audio/mp4a-latm"},
            {".m4u", "video/vnd.mpegurl"},
            {".m4v", "video/x-m4v"},
            {".mov", "video/quicktime"},
            {".mp2", "audio/x-mpeg"},
            {".mp3", "audio/x-mpeg"},
            {".mp4", "video/mp4"},
            {".mpc", "application/vnd.mpohun.certificate"},
            {".mpe", "video/mpeg"},
            {".mpeg", "video/mpeg"},
            {".mpg", "video/mpeg"},
            {".mpg4", "video/mp4"},
            {".mpga", "audio/mpeg"},
            {".msg", "application/vnd.ms-outlook"},
            {".ogg", "audio/ogg"},
            {".pdf", "application/pdf"},
            {".png", "image/png"},
            {".pps", "application/vnd.ms-powerpoint"},
            {".ppt", "application/vnd.ms-powerpoint"},
            {".prop", "text/plain"},
            {".rar", "application/x-rar-compressed"},
            {".rc", "text/plain"},
            {".rmvb", "audio/x-pn-realaudio"},
            {".rtf", "application/rtf"},
            {".sh", "text/plain"},
            {".tar", "application/x-tar"},
            {".tgz", "application/x-compressed"},
            {".txt", "text/plain"},
            {".wav", "audio/x-wav"},
            {".wma", "audio/x-ms-wma"},
            {".wmv", "audio/x-ms-wmv"},
            {".wps", "application/vnd.ms-works"},
            //{".xml",    "text/xml"},
            {".xml", "text/plain"},
            {".z", "application/x-compress"},
            {".zip", "application/zip"},
            {"", "*/*"}
    };


    /**
     * 获取文件的后缀
     *
     * @param file
     * @return
     */
    public static String getSuffix(File file) {
        if (file == null) {
            throw new NullPointerException("Please give me a correct data");
        }
        String fileName = file.getName();
        return fileName.substring(fileName.lastIndexOf(".") + 1);
    }

    public static final long KB = 1024;
    public static final long MB = KB * 1024;
    public static final long GB = MB * 1024;
    public static final long TB = GB * 1024;

    /**
     * 获取文件大小(G/M/K)
     *
     * @param size
     * @return
     */
    public static String getFileSize(long size) {
        if (size >= TB) {
            return String.format("%.1f TB", (float) size / TB);
        } else if (size >= GB) {
            return String.format("%.1f GB", (float) size / GB);
        } else if (size >= MB) {
            float value = (float) size / MB;
            return String.format(value > 100 ? "%.0f MB" : "%.1f MB", value);
        } else if (size >= KB) {
            float value = (float) size / KB;
            return String.format(value > 100 ? "%.0f KB" : "%.1f KB", value);
        } else {
            return String.format("%d B", size);
        }
    }

    /**
     * 获取存储空间的根文件夹
     *
     * @return
     */
    public static File getStorageRootDir() {
        return Environment.getExternalStorageDirectory();
    }
}
