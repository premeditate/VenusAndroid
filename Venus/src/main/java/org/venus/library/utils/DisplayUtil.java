package org.venus.library.utils;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/6
 * Note        TODO
 */
public class DisplayUtil {

    /**
     * 获取屏幕宽度
     * @param activity
     * @return
     */
    public static int getScreenWidth(Activity activity){
        return activity.getWindowManager().getDefaultDisplay().getWidth();
    }

    /**
     * 获取屏幕高度
     * @param activity
     * @return
     */
    public static int getScreenHeight(Activity activity){
        return activity.getWindowManager().getDefaultDisplay().getHeight();
    }

    /**
     * px转dip
     * @param context
     * @param px
     * @return
     */
    public  static int px2dip(Context context, float px){
        float density = context.getResources().getDisplayMetrics().density;

        return (int) (px / density + 0.5f);
    }

    /**
     * dp转px
     * @param context
     * @param dip
     * @return
     */
    public  static int dp2px(Context context, float dip){
        float density = context.getResources().getDisplayMetrics().density;

        return (int) (dip * density + 0.5f);
    }


    /**
     * sp转px
     * @param context
     * @param sp
     * @return
     */
    public  static int sp2px(Context context, float sp){
        float density = context.getResources().getDisplayMetrics().density;

        return (int) (sp * density + 0.5f);
    }

    /**
     * px转sp
     * @param context
     * @param px
     * @return
     */
    public  static int px2sp(Context context, float px){
        float density = context.getResources().getDisplayMetrics().density;

        return (int) (px / density + 0.5f);
    }



    /**
     * dp转px
     * @param dip
     * @return
     */
    public  static int dip2px_(Context context, float dip){

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dip,context.getResources().getDisplayMetrics());
    }

    /**
     * sp转px
     * @param sp
     * @return
     */
    public  static int sp2px_(Context context, float sp){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,sp,context.getResources().getDisplayMetrics());
    }
}
