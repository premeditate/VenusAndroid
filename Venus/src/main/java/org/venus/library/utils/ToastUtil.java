package org.venus.library.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Author      JinXing     _让世界看到我
 * On          2019/5/21
 * Note        TODO
 */

public class ToastUtil {

    private static Toast mToast;

    public static void show(Context context, Object obj){
        if (mToast == null){
            mToast = Toast.makeText(context,obj.toString(),Toast.LENGTH_SHORT);
        } else{
            mToast.setText(obj.toString());
        }
        mToast.show();
    }
}
