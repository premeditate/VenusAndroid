package org.venus.library.utils;


import android.os.Build;

import androidx.annotation.RequiresApi;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Base64;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/8/30
 * Note        TODO
 */
public class Base64Encoding {

    /**
     * 对字符串进行base64编码
     *
     * @param key
     * @return
     * @throws Exception
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String encoderBASE64(String key) throws Exception {
        Base64.Encoder encoder = Base64.getEncoder();
        byte[] textByte = key.getBytes("UTF-8");
        //编码
        String result = encoder.encodeToString(textByte);
        System.out.println("[" + key + "]进行base64编码结果为[" + result + "]");
        return result;
    }

    /**
     * 对字符串进行base64解码
     *
     * @param key
     * @return
     * @throws Exception
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String decoderBASE64(String key) throws Exception {
        Base64.Decoder decoder = Base64.getDecoder();
        //解码
        String result = new String(decoder.decode(key), "UTF-8");
        System.out.println("[" + key + "]进行base64解码结果为[" + result + "]");
        return result;
    }

    /**
     * 对URL进行编码
     *
     * @param key
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String encodeURIComponent(String key) throws UnsupportedEncodingException {
        // 对URL进行编码
        String result = URLEncoder.encode(key, "UTF-8")
                .replaceAll("\\+", "%20")
                .replaceAll("\\!", "%21")
                .replaceAll("\\'", "%27")
                .replaceAll("\\(", "%28")
                .replaceAll("\\)", "%29")
                .replaceAll("\\~", "%7E");
        System.out.println("[" + key + "]进行URL编码结果为[" + result + "]");
        return result;
    }

    /**
     * 对URL进行解码
     *
     * @param key
     * @return
     * @throws UnsupportedEncodingException
     */
    public static String decodeURIComponent(String key) throws UnsupportedEncodingException {
        // 对URL进行解码
        String result = URLDecoder.decode(key, "UTF-8");
        System.out.println("[" + key + "]进行URL解码结果为[" + result + "]");
        return result;
    }
}
