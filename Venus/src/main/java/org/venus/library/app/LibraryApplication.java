package org.venus.library.app;

import android.content.Context;

import androidx.multidex.MultiDexApplication;

/**
 * Author      JinXing     _让世界看到我
 * On          2018/11/20
 * Note        程序入口
 */

public class LibraryApplication extends MultiDexApplication {

    private static Context mContext;

    public static Context getContext() {
        return mContext;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        init();
    }

    private void init() {

    }
}
