package org.venus.library.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import org.venus.library.R;
import org.venus.library.utils.BASE64Encoder;
import org.venus.library.utils.ToastUtil;

import java.io.UnsupportedEncodingException;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @Author Jxx     让世界看到我
 * @Create 2022/4/21 9:47
 * @Note TODO
 */
public class PDFActivity extends AppCompatActivity {

    public static final String EXTRA_URL = "url";

    private String url;

    public static void join(Context context,String url){
        Intent intent = new Intent(context, PDFActivity.class);
        intent.putExtra(EXTRA_URL,url);
        context.startActivity(intent);
    }

    private  WebView mPdfView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        ButterKnife.bind(this);;

        mPdfView = findViewById(R.id.pdf_view);

        initData();
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent == null) {
            ToastUtil.show(this, "数据错误");
            finish();
            return;
        }
        try {
            url = intent.getStringExtra(EXTRA_URL);
            if (url == null){
                ToastUtil.show(this, "数据错误,请联系管理员");
                finish();
                return;
            }
            initView(url);
        } catch (Exception e) {
            e.printStackTrace();
            ToastUtil.show(this, "数据错误,请联系管理员");
            finish();
            return;
        }
    }

    private void initView(String url) {
        mPdfView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                // 返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                view.loadUrl(url);
                return true;
            }
        });
        WebSettings settings = mPdfView.getSettings();
        settings.setSavePassword(false);
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);
        settings.setBuiltInZoomControls(true);
        mPdfView.setWebChromeClient(new WebChromeClient());
        if (!"".equals(url)) {
            byte[] bytes = null;
            try {// 获取以字符编码为utf-8的字符
                bytes = url.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            if (bytes != null) {
                url = new BASE64Encoder().encode(bytes);// BASE64转码
            }
        }
        //下面是打开PDF的方法
        mPdfView.loadUrl("file:///android_asset/pdfjs/web/viewer.html?file=" + url);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }
}
