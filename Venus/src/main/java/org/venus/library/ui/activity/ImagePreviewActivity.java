package org.venus.library.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.chrisbanes.photoview.PhotoView;

import org.venus.library.R;
import org.venus.library.utils.GlideUtil;
import org.venus.library.utils.ToastUtil;

import butterknife.ButterKnife;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/12/2
 * Note        TODO
 */
public class ImagePreviewActivity extends AppCompatActivity {

    public static final String KEY_URL = "image_url";

    public static void join(Context context, String url) {
        Intent intent = new Intent(context, ImagePreviewActivity.class);
        intent.putExtra(KEY_URL, url);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        PhotoView mPhotoView = findViewById(R.id.photo_view);

        Intent intent = getIntent();
        if (intent == null) {
            finish();
        }
        String url = intent.getStringExtra(KEY_URL);
        if (url == null) {
            ToastUtil.show(this, "请给我一个正确的图片地址");
            finish();
            return;
        }
        GlideUtil.loadImage(this, url, mPhotoView);
    }
}
