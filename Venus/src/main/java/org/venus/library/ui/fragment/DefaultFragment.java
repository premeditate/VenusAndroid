package org.venus.library.ui.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/14
 * Note        TODO
 */
public class DefaultFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        TextView tv = new TextView(getActivity());
        tv.setTextColor(Color.RED);
        tv.setText("功能建设中...");
        tv.setGravity(Gravity.CENTER);
        return tv;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     *
     * fragment显示时回调,但是在onViewCreated之前,所以使用时要控制一下逻辑,
     *  如果是要每次显示时加载数据,那么加载数据的业务可以在这里和onViewCreated2个位置都执行.
     *
     * fragment切换时的回调,当界面显示时加载数据
     * 只有使用fragmentViewPager时才能使用
     * viewpager默认有预加载机制,但是有时候并不需要预加载,所以在这里加载数据
     * 但是不可靠,比如fragment是pagerAdapter中的第一个时,界面启动这个函数会在create函数之前调用,这样写会有错误,因为数据请求到了,往界面上显示时,界面还没有呢,所有用的时候要根据实际情况,这种写法并不通用
     * 可以在加一个flag,created中置为true,destroy中置为false,当界面创建了并且界面显示时加载数据,这样只能增加可靠性,但是并不合理,当第一个界面出现时就不能这样用,因为这个函数会在created之前回调,可能出现界面不加载数据,
     * 可以在created函数中也去判断这个条件同样加载一次数据
     *
     * @param isVisibleToUser
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }
}
