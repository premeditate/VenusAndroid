package org.venus.library.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.venus.library.Constant;
import org.venus.library.R;
import org.venus.library.utils.InputUtil;
import org.venus.library.utils.SharedPreferencesUtil;
import org.venus.library.utils.TVUtil;
import org.venus.library.utils.ToastUtil;

import butterknife.ButterKnife;
import cn.hutool.core.util.StrUtil;


/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/2
 * Note        TODO
 */
public abstract class BaseLoginActivity extends AppCompatActivity {

    TextView mTvTitle;
    ImageButton mBtnSetting;
    //MaterialEditText mEtUsername;
    EditText mEtUsername;
    ImageButton mBtnClearUsername;
    LinearLayout mL1;
    //MaterialEditText mEtPassword;
    EditText mEtPassword;
    ImageButton mBtnClearPassword;
    LinearLayout mL2;
    Button mBtnLogin;
    LinearLayout mCLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        initView();

        initEvent();

        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void initView() {
        mTvTitle = findViewById(R.id.tv_title);
        mBtnSetting = findViewById(R.id.btn_setting);
        mEtUsername = findViewById(R.id.et_username);
        mBtnClearUsername = findViewById(R.id.btn_clear_username);
        mL1 = findViewById(R.id.l1);
        mEtPassword = findViewById(R.id.et_password);
        mBtnClearPassword = findViewById(R.id.btn_clear_password);
        mL2 = findViewById(R.id.l2);
        mBtnLogin = findViewById(R.id.btn_login);
        mCLogin = findViewById(R.id.c_login);

        mBtnClearUsername.setVisibility(View.INVISIBLE);
        mBtnClearPassword.setVisibility(View.INVISIBLE);

        TVUtil.setText(mTvTitle,setTitle());

    }

    private void initEvent() {
        mBtnSetting.setOnClickListener(mOnClickListener);
        mBtnClearUsername.setOnClickListener(mOnClickListener);
        mBtnClearPassword.setOnClickListener(mOnClickListener);
        mBtnLogin.setOnClickListener(mOnClickListener);

        mEtUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    mBtnClearUsername.setVisibility(View.VISIBLE);
                } else {
                    mBtnClearUsername.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    mBtnClearPassword.setVisibility(View.VISIBLE);
                } else {
                    mBtnClearPassword.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initData() {
        TVUtil.setText(mEtUsername, SharedPreferencesUtil.getString(this, Constant.SP_LOGIN_USERNAME, Constant.SP_LOGIN_USERNAME_DEFAULT));
        TVUtil.setText(mEtPassword, SharedPreferencesUtil.getString(this, Constant.SP_LOGIN_PASSWORD, Constant.SP_LOGIN_PASSWORD_DEFAULT));
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            if (id == R.id.btn_setting) {
                joinSetting();
            } else if (id == R.id.btn_clear_username) {
                mEtUsername.setText("");
            } else if (id == R.id.btn_clear_password) {
                mEtPassword.setText("");
            } else if (id == R.id.btn_login) {
                login();
            }
        }
    };

    private void login() {
        String username = InputUtil.getInput(mEtUsername);
        String password = InputUtil.getInput(mEtPassword);

        if (StrUtil.isEmpty(username)) {
            ToastUtil.show(this, "请输入账号");
            return;
        }
        if (StrUtil.isEmpty(password)) {
            ToastUtil.show(this, "请输入密码");
            return;
        }

        login(username, password);
    }

    public abstract void login(String username, String password);

    public abstract void joinSetting();

    protected abstract String setTitle();
}
