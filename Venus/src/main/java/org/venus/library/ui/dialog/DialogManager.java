package org.venus.library.ui.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import org.venus.library.R;
import org.venus.library.ui.dialog.tip.TipDialog;
import org.venus.library.utils.TVUtil;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/4
 * Note        TODO
 */
public class DialogManager {

    private static TipDialog tipDialog;

    /**
     * 显示顶层加载对话框
     * @param activity
     */
    public static void showTipLoadingDialog(Activity activity){
        showTipLoadingDialog(activity,"数据处理中...");
    }

    /**
     * 显示顶层加载对话框
     * @param activity
     * @param text
     */
    public static void showTipLoadingDialog(Activity activity, String text){
        if (tipDialog != null && tipDialog.isShowing()){
            tipDialog.dismiss();
        }
        tipDialog = new TipDialog.Builder(activity)
                .setIconType(TipDialog.Builder.ICON_TYPE_LOADING)
                .setTipWord(text)
                .create();
        tipDialog.show();
    }

    /**
     * 关闭顶层加载对话框
     */
    public static void closeTipLoadingDialog(){
        if (tipDialog != null && tipDialog.isShowing()) {
            tipDialog.dismiss();
        }
    }

    /**
     * 显示是否退出对话框
     * @param activity
     * @param onExitDialogListener
     */
    public static void showExitDialog(Activity activity, final OnExitDialogListener onExitDialogListener){
        showExitDialog(activity,"提示","是否退出程序？",onExitDialogListener);
    }

    /**
     * 显示是否退出对话框
     * @param activity
     * @param title
     * @param message
     * @param onExitDialogListener
     */
    public static void showExitDialog(Activity activity, String title,String message, final OnExitDialogListener onExitDialogListener){
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        View contentView = View.inflate(activity, R.layout.dialog_exit,null);
        TextView tvTitle = contentView.findViewById(R.id.title);
        TextView tvMessage = contentView.findViewById(R.id.message);
        TextView cancel = contentView.findViewById(R.id.cancel);
        TextView affirm = contentView.findViewById(R.id.affirm);
        TVUtil.setText(tvTitle,title);
        TVUtil.setText(tvMessage,message);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        affirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onExitDialogListener != null){
                    onExitDialogListener.affirm();
                }
                dialog.dismiss();
            }
        });

        dialog.setContentView(contentView);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public interface OnExitDialogListener{
        void affirm();
    }

    public static void showAffirmDialog(Activity activity, String title, String message,OnAffirmDialogListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        AlertDialog dialog = builder
                .setTitle(title)
                .setIcon(R.drawable.icon_hint)
                .setMessage(message)
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (listener!=null){
                            listener.cancel();
                        }
                    }
                })
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if (listener!=null){
                            listener.affirm();
                        }
                    }
                })
                .create();

        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public interface OnAffirmDialogListener{
        void affirm();
        void cancel();
    }
}
