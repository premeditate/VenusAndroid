package org.venus.library.ui.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.tencent.smtt.export.external.interfaces.IX5WebChromeClient;
import com.tencent.smtt.export.external.interfaces.JsResult;
import com.tencent.smtt.sdk.CookieSyncManager;
import com.tencent.smtt.sdk.DownloadListener;
import com.tencent.smtt.sdk.WebChromeClient;
import com.tencent.smtt.sdk.WebSettings;
import com.tencent.smtt.sdk.WebView;
import com.tencent.smtt.sdk.WebViewClient;
import com.tencent.smtt.utils.TbsLog;

import org.venus.library.R;
import org.venus.library.ui.dialog.DialogManager;
import org.venus.library.utils.ToastUtil;
import org.venus.library.view.webview.X5WebView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/26
 * Note        TODO
 */
public class WebViewActivity extends AppCompatActivity {

    public static final String TAG_URL = "tag_url";

    private String otherUrl = "www.baidu.com";
    private String homeUrl;

    FrameLayout mCWebView;
    ImageButton mBtnBack;
    RelativeLayout mBtnCBack;
    ImageButton mBtnForward;
    RelativeLayout mBtnCForward;
    ImageButton mBtnMore;
    RelativeLayout mBtnCMore;
    ImageButton mBtnHome;
    RelativeLayout mBtnCHome;
    ImageButton mBtnQuit;
    RelativeLayout mBtnCQuit;
    LinearLayout mToolBar;
    ProgressBar mProgressBar;

    private X5WebView mWebView;

    public static void join(Context context, String url) {
        Intent intent = new Intent(context, WebViewActivity.class);
        intent.putExtra(TAG_URL, url);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);

        initView();

        initEvent();

        String url = initData();

        homeUrl = url;
        mWebView.loadUrl(url);
    }

    private void initEvent() {
        mBtnBack.setOnClickListener(mOnClickListener);
        mBtnCBack.setOnClickListener(mOnClickListener);
        mBtnForward.setOnClickListener(mOnClickListener);
        mBtnCForward.setOnClickListener(mOnClickListener);
        mBtnMore.setOnClickListener(mOnClickListener);
        mBtnCMore.setOnClickListener(mOnClickListener);
        mBtnHome.setOnClickListener(mOnClickListener);
        mBtnCHome.setOnClickListener(mOnClickListener);
        mBtnQuit.setOnClickListener(mOnClickListener);
        mBtnCQuit.setOnClickListener(mOnClickListener);
    }

    private void initView() {
        mCWebView = findViewById(R.id.c_web_view);
        mBtnBack = findViewById(R.id.btn_back);
        mBtnCBack = findViewById(R.id.btn_c_back);
        mBtnForward = findViewById(R.id.btn_forward);
        mBtnCForward = findViewById(R.id.btn_c_forward);
        mBtnMore = findViewById(R.id.btn_more);
        mBtnCMore = findViewById(R.id.btn_c_more);
        mBtnHome = findViewById(R.id.btn_home);
        mBtnCHome = findViewById(R.id.btn_c_home);
        mBtnQuit = findViewById(R.id.btn_quit);
        mBtnCQuit = findViewById(R.id.btn_c_quit);
        mToolBar = findViewById(R.id.tool_bar);
        mProgressBar = findViewById(R.id.progress_bar);

        initWebView();
    }

    private void initWebView() {
        mWebView = new X5WebView(this, null);

        mCWebView.addView(mWebView, new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));

        mProgressBar.setMax(100);
        mProgressBar.setProgressDrawable(this.getResources().getDrawable(R.drawable.color_progressbar));

        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println("shouldOverrideUrlLoading");
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                System.out.println("onPageFinished");
            }
        });

        mWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView webView, int i) {
                super.onProgressChanged(webView, i);
                if (i >= 100) {
                    mProgressBar.setVisibility(View.GONE);
                } else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    mProgressBar.setProgress(i);
                }
            }

            @Override
            public boolean onJsConfirm(WebView arg0, String arg1, String arg2, JsResult arg3) {
                System.out.println("onJsConfirm");
                return super.onJsConfirm(arg0, arg1, arg2, arg3);
            }

            View myVideoView;
            View myNormalView;
            IX5WebChromeClient.CustomViewCallback callback;

            /**
             * 全屏播放配置
             */
            @Override
            public void onShowCustomView(View view, IX5WebChromeClient.CustomViewCallback customViewCallback) {
                FrameLayout normalView = (FrameLayout) findViewById(R.id.web_filechooser);
                ViewGroup viewGroup = (ViewGroup) normalView.getParent();
                viewGroup.removeView(normalView);
                viewGroup.addView(view);
                myVideoView = view;
                myNormalView = normalView;
                callback = customViewCallback;
            }

            @Override
            public void onHideCustomView() {
                if (callback != null) {
                    callback.onCustomViewHidden();
                    callback = null;
                }
                if (myVideoView != null) {
                    ViewGroup viewGroup = (ViewGroup) myVideoView.getParent();
                    viewGroup.removeView(myVideoView);
                    viewGroup.addView(myNormalView);
                }
            }

            @Override
            public boolean onJsAlert(WebView arg0, String arg1, String arg2, JsResult arg3) {
                /**
                 * 这里写入你自定义的window alert
                 */
                return super.onJsAlert(null, arg1, arg2, arg3);
            }
        });

        mWebView.setDownloadListener(new DownloadListener() {

            @Override
            public void onDownloadStart(String arg0, String arg1, String arg2, String arg3, long arg4) {

                new AlertDialog.Builder(WebViewActivity.this)
                        .setTitle("请准备下载")
                        .setPositiveButton("确定",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        ToastUtil.show(WebViewActivity.this, "如果需要下载,请联系笔者做下载功能");
                                    }
                                })
                        .setNegativeButton("取消",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                })
                        .setOnCancelListener(
                                new DialogInterface.OnCancelListener() {

                                    @Override
                                    public void onCancel(DialogInterface dialog) {

                                    }
                                }).show();
            }
        });

        WebSettings webSetting = mWebView.getSettings();

        //设置允许跨域访问
        //这样无效
        /*if (Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.JELLY_BEAN) {
            //webSetting.setAllowFileAccessFromFileURLs(false);
            //webSetting.setAllowUniversalAccessFromFileURLs(true);
        }*/

        webSetting.setAllowFileAccess(true);
        webSetting.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        webSetting.setSupportZoom(true);
        webSetting.setBuiltInZoomControls(true);
        webSetting.setUseWideViewPort(true);
        webSetting.setSupportMultipleWindows(false);
        // webSetting.setLoadWithOverviewMode(true);
        webSetting.setAppCacheEnabled(false);
        // webSetting.setDatabaseEnabled(true);
        webSetting.setDomStorageEnabled(true);
        webSetting.setJavaScriptEnabled(true);
        webSetting.setGeolocationEnabled(true);
        webSetting.setAppCacheMaxSize(Long.MAX_VALUE);
        webSetting.setAppCachePath(this.getDir("appcache", 0).getPath());
        webSetting.setDatabasePath(this.getDir("databases", 0).getPath());
        webSetting.setGeolocationDatabasePath(this.getDir("geolocation", 0).getPath());
        // webSetting.setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);
        webSetting.setPluginState(WebSettings.PluginState.ON_DEMAND);
        // webSetting.setRenderPriority(WebSettings.RenderPriority.HIGH);
        // webSetting.setPreFectch(true);

        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().sync();
    }

    public String initData() {
        Intent intent = getIntent();
        if (intent == null) {
            ToastUtil.show(this, "数据错误");
            finish();
            return "";
        }

        String url = intent.getStringExtra(TAG_URL);
        if (url == null) {
            url = otherUrl;
        }

        return url;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            DialogManager.showExitDialog(this, "提示", "是否退出访问", new DialogManager.OnExitDialogListener() {
                @Override
                public void affirm() {
                    finish();
                }
            });
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.btn_back) {
                back();
            } else if (id == R.id.btn_c_back) {
                back();
            } else if (id == R.id.btn_forward) {
                forward();
            } else if (id == R.id.btn_c_forward) {
                forward();
            } else if (id == R.id.btn_more) {
                more();
            } else if (id == R.id.btn_c_more) {
                more();
            } else if (id == R.id.btn_home) {
                home();
            } else if (id == R.id.btn_c_home) {
                home();
            } else if (id == R.id.btn_quit) {
                quit();
            } else if (id == R.id.btn_c_quit) {
                quit();
            }
        }
    };

    public void quit() {
        DialogManager.showExitDialog(WebViewActivity.this, "提示", "是否退出访问", new DialogManager.OnExitDialogListener() {
            @Override
            public void affirm() {
                finish();
            }
        });
    }

    public void home() {
        if (mWebView != null) {
            mWebView.loadUrl(homeUrl);
        }
    }

    public void more() {
        ToastUtil.show(WebViewActivity.this, "暂时没有更多功能");
    }

    public void forward() {
        if (mWebView != null && mWebView.canGoForward()) {
            mWebView.goForward();
        }
    }

    public void back() {
        if (mWebView != null && mWebView.canGoBack()) {
            mWebView.goBack();
        }
    }

    public void setHomeUrl(String url){
        homeUrl = url;
    }
}
