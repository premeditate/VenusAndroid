package org.venus.library.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.venus.library.R;
import org.venus.library.utils.ToastUtil;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/12/2
 * Note        TODO
 */
public class VideoPlayerActivity extends AppCompatActivity {

    public static final String KEY_URL = "image_url";
    public static final String KEY_TITLE = "view_title";

    public static void join(Context context, String url, String title) {
        Intent intent = new Intent(context, VideoPlayerActivity.class);
        intent.putExtra(KEY_URL, url);
        intent.putExtra(KEY_TITLE, title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);

        Intent intent = getIntent();
        if (intent == null) {
            finish();
            return;
        }
        String url = intent.getStringExtra(KEY_URL);
        String title = intent.getStringExtra(KEY_TITLE);
        if (url == null || url.length()<1) {
            ToastUtil.show(this, "请给我一个正确的视频地址");
            finish();
            return;
        }

        if (title != null && title.length() > 0) {
            ToastUtil.show(VideoPlayerActivity.this, title);
        }

        VideoView mVideo = findViewById(R.id.video);

        //设置媒体控制器
        mVideo.setMediaController(new MediaController(this));
        //设置播放文成的回调接口

        mVideo.setOnCompletionListener(mOnCompletionListener);
        //设置播放异常的回调接口
        mVideo.setOnErrorListener(mOnErrorListener);
        mVideo.setVideoURI(Uri.parse(url));
        //开始播放
        mVideo.start();
    }

    MediaPlayer.OnCompletionListener mOnCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mp) {
            ToastUtil.show(VideoPlayerActivity.this, "播放完成");
        }
    };

    MediaPlayer.OnErrorListener mOnErrorListener = new MediaPlayer.OnErrorListener() {
        @Override
        public boolean onError(MediaPlayer mp, int what, int extra) {
            ToastUtil.show(VideoPlayerActivity.this, "播放错误");
            return false;
        }
    };
}
