package org.venus.library.ui.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.venus.library.Constant;
import org.venus.library.R;
import org.venus.library.utils.InputUtil;
import org.venus.library.utils.SharedPreferencesUtil;
import org.venus.library.utils.TVUtil;
import org.venus.library.utils.ToastUtil;
import org.venus.library.view.materialedittext.MaterialEditText;

import butterknife.ButterKnife;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.StrUtil;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/2
 * Note        TODO
 */
public abstract class BaseSettingActivity extends AppCompatActivity {


    ImageButton mBtnClearIp;
    TextView mHintIp;
    TextView mTvTitle;
    MaterialEditText mEtIp;
    ImageButton mBtnClearPort;
    TextView mHintPort;
    MaterialEditText mEtPort;
    Button mBtnAffirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);

        initView();
        initEvent();
        initData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void initData() {
        TVUtil.setText(mEtIp, SharedPreferencesUtil.getString(this, Constant.SP_SERVER_IP, Constant.SP_SERVER_IP_DEFAULT));
        TVUtil.setText(mEtPort, SharedPreferencesUtil.getInt(this, Constant.SP_SERVER_PORT, Constant.SP_SERVER_PORT_DEFAULT));
    }

    private void initEvent() {
        mBtnClearIp.setOnClickListener(mOnClickListener);
        mBtnClearPort.setOnClickListener(mOnClickListener);
        mBtnAffirm.setOnClickListener(mOnClickListener);

        mEtIp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    mBtnClearIp.setVisibility(View.VISIBLE);
                } else {
                    mBtnClearIp.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEtPort.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() > 0) {
                    mBtnClearPort.setVisibility(View.VISIBLE);
                } else {
                    mBtnClearPort.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initView() {
        mTvTitle = findViewById(R.id.tv_title);
        mBtnClearIp = findViewById(R.id.btn_clear_ip);
        mHintIp = findViewById(R.id.hint_ip);
        mEtIp = findViewById(R.id.et_ip);
        mBtnClearPort = findViewById(R.id.btn_clear_port);
        mHintPort = findViewById(R.id.hint_port);
        mEtPort = findViewById(R.id.et_port);
        mBtnAffirm = findViewById(R.id.btn_affirm);

        mBtnClearIp.setVisibility(View.GONE);
        mBtnClearPort.setVisibility(View.GONE);

        TVUtil.setText(mTvTitle,setTitle());
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int id = v.getId();
            if (id == R.id.btn_clear_ip) {
                TVUtil.setText(mEtIp, "");
            } else if (id == R.id.btn_clear_port) {
                TVUtil.setText(mEtPort, "");
            } else if (id == R.id.btn_affirm) {
                affirm();
            }
        }
    };

    private void affirm() {
        String ip = InputUtil.getInput(mEtIp);
        String portS = InputUtil.getInput(mEtPort);

        if (StrUtil.isEmpty(ip)) {
            ToastUtil.show(this, "请输入服务器地址");
            return;
        }

        if (!Validator.isIpv4(ip)){
            ToastUtil.show(this, "请输入正确的服务器IPv4地址");
            return;
        }

        if (StrUtil.isEmpty(portS)) {
            ToastUtil.show(this, "请输入服务器端口");
            return;
        }

        int port = -1;
        try {
            port = Integer.parseInt(portS);
        } catch (Exception e) {

        }

        if (port < 80 || port > 65535) {
            ToastUtil.show(this, "请输入正确的端口,80~65535");
            return;
        }

        affirm(ip,port);
    }

    public abstract void affirm(String ip, int port);

    protected abstract String setTitle();
}
