package org.venus.library;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/6/17
 * Note        TODO
 */
public class Constant {

    public static final String ADMIN_USERNAME = "admin";
    public static final String ADMIN_PASSWORD = "admin";

    public static final String SP_LOGIN_USERNAME = "login_username";
    public static final String SP_LOGIN_USERNAME_DEFAULT = "admin";
    public static final String SP_LOGIN_PASSWORD = "login_password";
    public static final String SP_LOGIN_PASSWORD_DEFAULT = "admin";

    public static final String SP_SERVER_IP = "server_ip";
    public static final String SP_SERVER_IP_DEFAULT = "192.168.101.101";
    public static final String SP_SERVER_PORT = "server_port";
    public static final int SP_SERVER_PORT_DEFAULT = 8080;
}
