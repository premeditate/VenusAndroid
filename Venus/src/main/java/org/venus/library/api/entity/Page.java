package org.venus.library.api.entity;

import java.util.List;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/8/27
 * Note        TODO
 */
public class Page<T> {
    public List<T> records;
    public long total;
    public long size;
    public long current;
}
