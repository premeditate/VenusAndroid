package org.venus.library.api;

import org.venus.library.api.entity.Result;
import org.venus.library.app.LibraryApplication;
import org.venus.library.utils.ToastUtil;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/7/21
 * Note        TODO
 */
public abstract class RetrofitCallback<T> implements Callback<Result<T>> {

    @Override
    public void onResponse(Call<Result<T>> call, Response<Result<T>> response) {
        back();
        if (response.code() == 200) {
            Result body = response.body();
            if (body.code == Result.SUCCESS_CODE) {
                T data = (T) body.data;
                success(call, data);
            } else {
                errorData(call, body);
            }
        } else {
            errorServer(call, response);
        }
    }

    @Override
    public void onFailure(Call<Result<T>> call, Throwable throwable) {
        back();
        errorNet(call, throwable);
    }

    //成功
    public abstract void success(Call<Result<T>> call, T data);

    /**
     * 网络错误
     *
     * @param call
     * @param throwable
     */
    public void errorNet(Call<Result<T>> call, Throwable throwable) {
        System.out.println(call.request().url() + "网络错误:" + throwable.getClass() + ":" + throwable.getMessage());
        ToastUtil.show(LibraryApplication.getContext(),"网络错误:" + throwable.getClass() + ":" + throwable.getMessage());
        throwable.printStackTrace();
    }

    /**
     * 服务器错误
     *
     * @param call
     * @param response
     */
    public void errorServer(Call<Result<T>> call, Response<Result<T>> response) {
        System.out.println("服务器错误:" + response.code());
        ToastUtil.show(LibraryApplication.getContext(),"服务器错误:" + response.code());
    }

    /**
     * 数据错误
     *
     * @param call
     * @param body
     */
    public void errorData(Call<Result<T>> call, Result body) {
        System.out.println("数据错误:" + body.msg);
        ToastUtil.show(LibraryApplication.getContext(),"数据错误:" + body.msg);
    }

    //返回主线程
    public void back() {
        System.out.println("网络请求结束,返回主线程");
    }
}
