package org.venus.library.api.entity;
import java.io.Serializable;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/1/19
 * Note        TODO
 */
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    //服务器请求成功的编号
    public static final int SUCCESS_CODE = 0;

    public String msg;

    public int code;

    public T data;
}
