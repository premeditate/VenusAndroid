package org.venus.library.net;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/1/22
 * Note        TODO
 */
public class HttpRetrofit {

    private static Retrofit mRetrofit;

    private HttpRetrofit() {
    }

    public static Retrofit getInstance(String baseUrl) {
        if (mRetrofit == null) {
            synchronized (HttpRetrofit.class) {
                if (mRetrofit == null) {
                    mRetrofit = new Retrofit.Builder()
                            .baseUrl(baseUrl)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build();
                }
            }
        }
        return mRetrofit;
    }

    public static void setNull() {
        mRetrofit = null;
    }
}
