package org.venus;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.huawei.hms.hmsscankit.ScanUtil;
import com.huawei.hms.ml.scan.HmsScan;
import com.huawei.hms.ml.scan.HmsScanAnalyzerOptions;

import org.venus.library.ui.activity.PDFActivity;
import org.venus.library.ui.activity.WebViewActivity;
import org.venus.library.ui.dialog.DialogManager;
import org.venus.library.utils.TVUtil;
import org.venus.ui.activity.demo.LoadMoreActivity2;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {


    public static final int REQUEST_CODE_SCAN_ONE = 1001;
    public static final int CAMERA_REQ_CODE = 1002;

    @Bind(R.id.btn_table)
    Button mBtnTable;
    @Bind(R.id.btn_web)
    Button mBtnWeb;
    @Bind(R.id.btn_load_more)
    Button mBtnLoadMore;
    @Bind(R.id.tv_show_scan)
    TextView mTvShowScan;

    public static void join(Context context) {
        context.startActivity(new Intent(context, MainActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            DialogManager.showExitDialog(this, new DialogManager.OnExitDialogListener() {
                @Override
                public void affirm() {
                    finish();
                }
            });
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @OnClick({R.id.btn_table, R.id.btn_web,R.id.btn_load_more,R.id.btn_pdf,R.id.btn_scan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_table:
                break;
            case R.id.btn_web:
                String url = "www.baidu.com";
                WebViewActivity.join(this,url);
                break;
            case R.id.btn_load_more:
                LoadMoreActivity2.join(this);
                break;
            case R.id.btn_pdf:
                PDFActivity.join(this,"https://www.hikrobotics.com/cn2/source/vision/document/2022/4/21/%E6%B5%B7%E5%BA%B7%E6%9C%BA%E5%99%A8%E4%BA%BAUSB3.0%E5%B7%A5%E4%B8%9A%E9%9D%A2%E9%98%B5%E7%9B%B8%E6%9C%BA%E7%94%A8%E6%88%B7%E6%89%8B%E5%86%8CV2.1.2.pdf");
                break;
            case R.id.btn_scan:
                requestPermission(CAMERA_REQ_CODE, 1);
                break;
        }
    }


    private void requestPermission(int requestCode, int mode) {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (permissions == null || grantResults == null) {
            return;
        }
        if (grantResults.length < 2 || grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (requestCode == CAMERA_REQ_CODE) {
            ScanUtil.startScan(this, REQUEST_CODE_SCAN_ONE, new HmsScanAnalyzerOptions.Creator().setHmsScanTypes(HmsScan.QRCODE_SCAN_TYPE).create());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK || data == null) {
            return;
        }
        if (requestCode == REQUEST_CODE_SCAN_ONE) {
            HmsScan obj = data.getParcelableExtra(ScanUtil.RESULT);
            if (obj==null){
                TVUtil.setText(mTvShowScan,"扫描结果空");
                return;
            }
            TVUtil.setText(mTvShowScan,obj.originalValue);
        }
    }
}
