package org.venus.ui.activity.demo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.venus.R;
import org.venus.library.utils.ToastUtil;
import org.venus.library.view.loadmore.LoadMoreLayout;
import org.venus.ui.activity.adapter.LoadMore;
import org.venus.ui.activity.adapter.LoadMoreListAdapter;
import org.venus.ui.activity.mock.MockLoadMore;

import java.util.List;

import butterknife.ButterKnife;


/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/26
 * Note        TODO
 */
public class LoadMoreActivity2 extends AppCompatActivity {

    private RelativeLayout mC;
    private LoadMoreLayout mLoadMoreLayout;

    private LoadMoreListAdapter mLoadMoreListAdapter;

    private int count = 20;

    public static void join(Context context) {
        context.startActivity(new Intent(context, LoadMoreActivity2.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_more2);
        ButterKnife.bind(this);

        initView();

        initEvent();

        initData();
    }

    private void initEvent() {
        mLoadMoreLayout.setOnEventListener(mOnEventListener);
    }

    private void onLoadMoreStart() {
        //linearLayoutManager.scrollToPosition(linearLayoutManager.getItemCount()-1);
        //mLoadMoreLayout.scrollToLast();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    System.out.println("total:"+mLoadMoreListAdapter.getItemCount());
                    List<LoadMore> list = MockLoadMore.getLoadMoreList(count,mLoadMoreListAdapter.getItemCount());
                    Thread.sleep(2000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //更新数据
                            onLoadMoreSuccess(list);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    private void onLoadMoreSuccess(List<LoadMore> list) {
        System.out.println("上拉加载更多完成");
        mLoadMoreListAdapter.addData(list);

        mLoadMoreLayout.onLoadMoreStop();
        if (list.size() < 20) {
            //ToastUtil.show(this,"没有更多数据了..");
        }
    }

    private void onRefreshSuccess(List<LoadMore> list) {
        System.out.println("下拉刷新完成");
        mLoadMoreListAdapter.setData(list);

        mLoadMoreLayout.onRefreshStop();
    }

    private void onRefreshStart() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    List<LoadMore> list = MockLoadMore.getRefreshList(count);
                    Thread.sleep(2000);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //更新数据
                            onRefreshSuccess(list);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    private void initData() {
        List<LoadMore> list = MockLoadMore.getList(20);

        showData(list);
    }

    private void initView() {
        mC = findViewById(R.id.c);

        mLoadMoreLayout = new LoadMoreLayout(this);

        mC.addView(mLoadMoreLayout);

        mLoadMoreLayout.setLoadText("代码创建的加载更多");

    }

    LoadMoreLayout.OnEventListener mOnEventListener = new LoadMoreLayout.OnEventListener() {
        @Override
        public void onRefresh() {
            System.out.println("使用地方的开始刷新");
            onRefreshStart();
        }

        @Override
        public void onLoadMore() {
            System.out.println("使用地方的开始加载更多");
            onLoadMoreStart();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void showData(List<LoadMore> list) {
        if (mLoadMoreListAdapter == null) {
            mLoadMoreListAdapter = new LoadMoreListAdapter(this);
        }

        mLoadMoreListAdapter.setData(list);
        //mList.setAdapter(mLoadMoreListAdapter);

        mLoadMoreLayout.setRecyclerViewAdapter(mLoadMoreListAdapter);
    }
}
