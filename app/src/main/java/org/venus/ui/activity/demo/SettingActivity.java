package org.venus.ui.activity.demo;

import android.content.Context;
import android.content.Intent;

import org.venus.R;
import org.venus.library.Constant;
import org.venus.library.ui.activity.BaseSettingActivity;
import org.venus.library.utils.SharedPreferencesUtil;
import org.venus.library.utils.ToastUtil;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/2
 * Note        TODO
 */
public class SettingActivity extends BaseSettingActivity {

    public static void join(Context context){
        context.startActivity(new Intent(context,SettingActivity.class));
    }

    @Override
    public void affirm(String ip, int port) {
        SharedPreferencesUtil.setString(this, Constant.SP_SERVER_IP, ip);
        SharedPreferencesUtil.setInt(this, Constant.SP_SERVER_PORT, port);
        ToastUtil.show(this, "设置成功");
        finish();
    }

    @Override
    protected String setTitle() {
        return getResources().getString(R.string.setting_title);
    }
}
