package org.venus.ui.activity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.venus.R;
import org.venus.library.utils.TVUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/26
 * Note        TODO
 */
public class LoadMoreListAdapter extends RecyclerView.Adapter {

    private Context mContext;

    private List<LoadMore> data;

    public LoadMoreListAdapter(Context context) {
        mContext = context;
    }

    public void setData(List<LoadMore> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void addData(List<LoadMore> data) {
        if (this.data==null){
            return;
        }
        if (data==null){
            return;
        }
        this.data.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(mContext).inflate(R.layout.item_load_more, parent, false);
        return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.setData(position);
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tv_title)
        TextView mTvTitle;
        @Bind(R.id.tv_id)
        TextView mTvId;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        public void setData(int position) {
            LoadMore loadMore = data.get(position);
            TVUtil.setText(mTvTitle,loadMore.name);
            TVUtil.setText(mTvId,position);
        }
    }
}
