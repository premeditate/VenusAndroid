package org.venus.ui.activity.mock;

import org.venus.library.utils.DateUtil;
import org.venus.ui.activity.adapter.LoadMore;

import java.util.ArrayList;
import java.util.List;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/26
 * Note        TODO
 */
public class MockLoadMore {

    public static List<LoadMore> getList(int count) {
        List<LoadMore> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            LoadMore loadMore = new LoadMore();
            loadMore.name = "第" + i + "个-" + DateUtil.format(System.currentTimeMillis());
            list.add(loadMore);
        }
        return list;
    }

    public static List<LoadMore> getRefreshList(int count) {
        List<LoadMore> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            LoadMore loadMore = new LoadMore();
            loadMore.name = "下拉刷新的第" + i + "个-" + DateUtil.format(System.currentTimeMillis());
            list.add(loadMore);
        }
        return list;
    }

    public static List<LoadMore> getLoadMoreList(int count, int total) {
        if (total > 90) {
            count -= 5;
        }
        System.out.println(count+"-"+total);
        List<LoadMore> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            LoadMore loadMore = new LoadMore();
            loadMore.name = "上拉加载更多的第" + i + "个-" + DateUtil.format(System.currentTimeMillis());
            list.add(loadMore);
        }
        return list;
    }
}
