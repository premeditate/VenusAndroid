package org.venus.ui.activity.demo;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import org.venus.R;
import org.venus.library.view.loadmore.LoadMoreLayout;
import org.venus.ui.activity.adapter.LoadMore;
import org.venus.ui.activity.adapter.LoadMoreListAdapter;
import org.venus.ui.activity.mock.MockLoadMore;

import java.util.List;

import butterknife.ButterKnife;


/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/26
 * Note        TODO
 */
public class LoadMoreActivity extends AppCompatActivity {

    private LoadMoreLayout mCLoadMoreLayout;

    private LoadMoreListAdapter mLoadMoreListAdapter;

    public static void join(Context context) {
        context.startActivity(new Intent(context, LoadMoreActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_more);
        ButterKnife.bind(this);

        initView();

        initEvent();

        initData();
    }

    private void initEvent() {
        mCLoadMoreLayout.setOnEventListener(mOnEventListener);
    }

    private void onLoadMoreStart() {
        //linearLayoutManager.scrollToPosition(linearLayoutManager.getItemCount()-1);
        mCLoadMoreLayout.scrollToLast();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    List<LoadMore> list = MockLoadMore.getLoadMoreList(20, mLoadMoreListAdapter.getItemCount());
                    Thread.sleep(2000);
                    //更新数据
                    onLoadMoreSuccess(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    private void onLoadMoreSuccess(List<LoadMore> list) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLoadMoreListAdapter.addData(list);
                mCLoadMoreLayout.onLoadMoreStop();
            }
        });
    }

    private void onRefreshSuccess(List<LoadMore> list) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mLoadMoreListAdapter.setData(list);

                mCLoadMoreLayout.onRefreshStop();
            }
        });
    }

    private void onRefreshStart() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    List<LoadMore> list = MockLoadMore.getRefreshList(20);
                    Thread.sleep(2000);
                    //更新数据
                    onRefreshSuccess(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    private void initData() {
        List<LoadMore> list = MockLoadMore.getList(20);

        showData(list);
    }

    private void initView() {
        mCLoadMoreLayout = findViewById(R.id.c_load_more);

        mCLoadMoreLayout.setLoadText("自定义的控件的上拉加载更多");

    }

    LoadMoreLayout.OnEventListener mOnEventListener = new LoadMoreLayout.OnEventListener() {
        @Override
        public void onRefresh() {
            System.out.println("使用地方的开始刷新");
            onRefreshStart();
        }

        @Override
        public void onLoadMore() {
            System.out.println("使用地方的开始加载更多");
            onLoadMoreStart();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void showData(List<LoadMore> list) {
        if (mLoadMoreListAdapter == null) {
            mLoadMoreListAdapter = new LoadMoreListAdapter(this);
        }

        mLoadMoreListAdapter.setData(list);
        //mList.setAdapter(mLoadMoreListAdapter);

        mCLoadMoreLayout.setRecyclerViewAdapter(mLoadMoreListAdapter);
    }
}
