package org.venus.ui.activity.demo;

import org.venus.MainActivity;
import org.venus.R;
import org.venus.library.Constant;
import org.venus.library.ui.activity.BaseLoginActivity;
import org.venus.library.utils.SharedPreferencesUtil;
import org.venus.library.utils.ToastUtil;

/**
 * Author      Jxx          _让世界看到我
 * On          2021/9/2
 * Note        TODO
 */
public class LoginActivity extends BaseLoginActivity {
    @Override
    public void login(String username, String password) {
        if (!(Constant.ADMIN_USERNAME.equals(username)&&Constant.ADMIN_PASSWORD.equals(password))){
            ToastUtil.show(this, "账号或者密码错误,请重新输入");
            return;
        }

        SharedPreferencesUtil.setString(this, Constant.SP_LOGIN_USERNAME, username);
        SharedPreferencesUtil.setString(this, Constant.SP_LOGIN_PASSWORD, password);
        ToastUtil.show(this, "登录成功");
        MainActivity.join(this);
        finish();
    }

    @Override
    public void joinSetting() {
        SettingActivity.join(this);
    }

    @Override
    protected String setTitle() {
        return getResources().getString(R.string.login_title);
    }
}
